package com.wk.client.balance.holder.controller;

import com.google.gson.Gson;
import com.wk.client.balance.holder.config.AppConfig;
import com.wk.client.balance.holder.config.WebInitializationConfig;
import com.wk.client.balance.holder.dto.ClientInfoDto;
import com.wk.client.balance.holder.entity.ClientInfo;
import com.wk.client.balance.holder.repository.ClientInfoRepository;
import com.wk.client.balance.holder.rest.ClientsInfoResponse;
import com.wk.client.balance.holder.service.ClientService;
import com.wk.client.balance.holder.service.impl.ClientServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class MainControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Autowired
    private ClientInfoRepository clientInfoRepository;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getClientsResultParsingTest() throws Exception {

        List<ClientInfo> clientInfoList = clientInfoRepository.findAllClients();

        MvcResult result = mockMvc.perform(
                get("/clients")
                        .accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        ClientsInfoResponse clientsInfoResponse = new Gson().fromJson(content, ClientsInfoResponse.class);

        List<ClientInfoDto> clientsInfoDtoList = clientsInfoResponse.getClientsInfo();
        assertEquals(clientInfoList.size(), clientsInfoDtoList.size());
    }

}