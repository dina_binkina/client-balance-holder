package com.wk.client.balance.holder.service.impl;

import com.wk.client.balance.holder.dto.ClientInfoDto;
import com.wk.client.balance.holder.entity.ClientInfo;
import com.wk.client.balance.holder.repository.ClientInfoRepository;
import com.wk.client.balance.holder.service.ClientService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientInfoRepository clientInfoRepository;

    public List<ClientInfoDto> getClients() {
        List<ClientInfoDto> returnValues = new ArrayList<>();
        Iterable<ClientInfo> clientInfoEntityList = clientInfoRepository.findAllClients();

        clientInfoEntityList.forEach(clientInfoEntity -> {
            ClientInfoDto returnValue = new ClientInfoDto();
            BeanUtils.copyProperties(clientInfoEntity, returnValue);
            returnValues.add(returnValue);
        });

        return returnValues;
    }

    public ClientInfoDto updateClient(ClientInfoDto clientInfoDto){
        ClientInfoDto returnValue = new ClientInfoDto();
        ClientInfo clientInfo = new ClientInfo();
        BeanUtils.copyProperties(clientInfoDto, clientInfo);
        ClientInfo updatedClientInfo = clientInfoRepository.updateClient(clientInfo);

        BeanUtils.copyProperties(updatedClientInfo, returnValue);
        return returnValue;
    }
}
