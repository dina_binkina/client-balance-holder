package com.wk.client.balance.holder.controller;

import com.wk.client.balance.holder.dto.ClientInfoDto;
import com.wk.client.balance.holder.rest.ClientsInfoResponse;
import com.wk.client.balance.holder.service.ClientService;
import com.wk.client.balance.holder.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MainController {

    @Autowired
    ClientService clientService;


    @GetMapping(path = "clients",
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            })
    public ResponseEntity<ClientsInfoResponse> getClientsInfo() {
        List<ClientInfoDto> clientsInfoList = clientService.getClients();

        ClientsInfoResponse response = new ClientsInfoResponse(clientsInfoList);
        return new ResponseEntity<ClientsInfoResponse>(response, HttpStatus.OK);
    }

    @GetMapping
    public String greetings() {
        return "Welcome to client-balance-holder app";
    }

}
