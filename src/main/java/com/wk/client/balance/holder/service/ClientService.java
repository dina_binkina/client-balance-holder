package com.wk.client.balance.holder.service;

import com.wk.client.balance.holder.dto.ClientInfoDto;

import java.util.List;

public interface ClientService {
    List<ClientInfoDto> getClients();
    ClientInfoDto updateClient(ClientInfoDto clientInfoDto);
}
