package com.wk.client.balance.holder.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientInfoDto {
    private String clientId;
    private String login;
    private Long tariff;
    private Long balance;
}
