package com.wk.client.balance.holder.repository;

import com.wk.client.balance.holder.entity.ClientInfo;

import java.util.List;

public interface ClientInfoRepository {
    List<ClientInfo> findAllClients();
    ClientInfo updateClient(ClientInfo clientInfo);
}
