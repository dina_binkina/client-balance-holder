package com.wk.client.balance.holder.rest;

import com.wk.client.balance.holder.dto.ClientInfoDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ClientsInfoResponse {

    private List<ClientInfoDto> clientsInfo;

    public ClientsInfoResponse(List<ClientInfoDto> clientsInfo) {
        this.clientsInfo = clientsInfo;
    }
}
