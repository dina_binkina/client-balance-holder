package com.wk.client.balance.holder.config;


import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
@EnableWebMvc
@Configuration
@ComponentScan("com.wk.client.balance.holder")
@PropertySource("classpath:db.properties")
public class AppConfig implements InitializingBean {

    @Value("${database.url}")
    private String jdbcUrl;

    @Value("${database.username}")
    private String userName;

    @Value("${database.password}")
    private String password;

    @Value("${database.driverClassName}")
    private String driverClassName;

    @Override
    public void afterPropertiesSet() throws Exception {
        runLiquibase();
    }

    public void runLiquibase() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        } catch (Exception e1) {
            log.error("Failed to load driver. {}", e1.getMessage());
        }

        Liquibase liquibase = null;
        Connection c = null;
        try {
            c = DriverManager.getConnection(jdbcUrl, userName, password);

            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(c));
            liquibase = new Liquibase("liquibase/master.xml", new ClassLoaderResourceAccessor(), database);
            liquibase.update("main");
        } catch (SQLException | LiquibaseException e) {
            e.getLocalizedMessage();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.getLocalizedMessage();
                }
            }
        }
    }

    @Bean
    public DataSource getDataSource() {
        DataSource dataSource = new DriverManagerDataSource();
        ((DriverManagerDataSource) dataSource).setDriverClassName(driverClassName);
        ((DriverManagerDataSource) dataSource).setUrl(jdbcUrl);
        ((DriverManagerDataSource) dataSource).setUsername(userName);
        ((DriverManagerDataSource) dataSource).setPassword(password);
        return dataSource;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        // JPA settings
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(true);
        vendorAdapter.setDatabase(org.springframework.orm.jpa.vendor.Database.MYSQL);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.wk.client.balance.holder");
        factory.setDataSource(getDataSource());
        factory.afterPropertiesSet();
        return factory.getObject();
    }
}
