package com.wk.client.balance.holder.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name="CLIENT_BALANCE_INFO")
public class ClientInfo {
    @Id
    private Long id;
    private String login;
    private Long tariff;
    private Long balance;
    private String clientId;
}
