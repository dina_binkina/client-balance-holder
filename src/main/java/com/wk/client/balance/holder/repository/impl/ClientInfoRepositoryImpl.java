package com.wk.client.balance.holder.repository.impl;

import com.wk.client.balance.holder.entity.ClientInfo;
import com.wk.client.balance.holder.repository.ClientInfoRepository;
import com.wk.client.balance.holder.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Component
public class ClientInfoRepositoryImpl implements ClientInfoRepository {
    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    Utils utils;

    public List<ClientInfo> findAllClients() {
        EntityManager em = entityManagerFactory.createEntityManager();
        TypedQuery<ClientInfo> query = em.createQuery("SELECT e FROM ClientInfo e", ClientInfo.class);

        List<ClientInfo> resultList = query.getResultList();
       for(ClientInfo clientInfo: resultList) {
           if (clientInfo.getClientId() == null) {
               clientInfo.setClientId(utils.generateClientId(9));
               updateClient(clientInfo);
           }
       }
        return resultList;
    }

    public ClientInfo updateClient(ClientInfo clientInfo) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        ClientInfo updatedClientInfo = em.merge(clientInfo);
        em.getTransaction().commit();
        return updatedClientInfo;
    }
}
